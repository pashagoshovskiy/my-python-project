# 1.
a = 2 * 3
b = (3 * 3 + 8)/3
c = 8 // 3
d = 8 % 3
e = 5 ** 2

print(a, b, c, d, e)
print('Hello' + 'world')

# 2.
name = 'Pasha'
sentence = "no money no honey"

# 3.
res1 = sentence[:10]
res2 = sentence[3:13]
res3 = sentence[len(sentence) - 10:]
res4 = sentence[::-1]
res5 = sentence[0::2]
res6 = sentence[1::2]

print(res1)
print(res2)
print(res3)
print(res4)
print(res5)
print(res6)

# 4.
first = input('Enter first number:')
second = input('Enter second number:')
result = float(first) + float(second)

print(result)
