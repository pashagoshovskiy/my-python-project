# Set methods

# 'difference_update'
a = {"UA", "GBR", "D", "BY", "FR", "IT"}
b = {"HG", "ES", "SWI", "IT", "JAP", "FR"}
# a.difference_update(b)
# print(a)

# 'discard'
a.discard('GBR')
print(a)
b.discard("SWI")
print(b)

# 'intersection_update'
b.intersection_update(a)
print(b)

# 'isdisjoint'
f = {"FR", "IT"}
g = {}
x = f.isdisjoint(g)
print(x)

# 'issubset'
c = {"FR", "IT"}
d = {"ES", "SWI", "IT", "JAP", "FR"}
y = c.issubset(d)
print(y)

# 'issuperset'
p = {"ES", "SWI", "IT", "JAP", "FR"}
w = {"FR", "IT"}
k = p.issuperset(w)
print(k)

# 'symmetric_difference'
j = {"UA", "GBR", "D", "BY", "FR", "IT"}
m = {"HG", "ES", "SWI", "JAP", "IT", "FR"}
u = j.symmetric_difference(m)
print(u)

# 'symmetric_difference_update'
j.symmetric_difference_update(m)
print(j)
m.symmetric_difference_update(j)
print(m)
# уточнить результат m, не до конца понял почему

# 'union'
result = a.union(j, m)
print(result)
