from pizzastore.objects.pizzastore import PizzaStore
from pizzastore.objects.receipt import Receipt


def main(pizzastore):
    while True:
        choice = input("Enter your choice or press 0 to exit: ")
        match choice:
            case "0":
                break
            case "1":
                print('Our menu:')
                pizzastore.print_menu()
            case "2":
                receipt = Receipt()
                receipt.receipt_print(pizzastore.pizzas)
                pizzastore.receipts.append(receipt)
                print(receipt)
            case "3":
                thr = 110
                print(f"Pizzas less than {thr} UAH:")
                filtered = pizzastore.filter_pizzas(money=thr)
                pizzastore.pizzas_print(filtered)
            case "4":
                print("Pizza menu sorted by price:")
                pizzastore.pizzas_sorted_by_price()
            case _:
                print("Wrong Choice")


if __name__ == "__main__":
    pizzastore = PizzaStore()
    main(pizzastore)
