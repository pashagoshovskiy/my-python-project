from datetime import datetime
import random
from pizzastore.objects.receiptline import ReceiptLine


class Receipt:
    orders = 1

    def __init__(self):
        self.datetime = datetime.now()
        self.lines = []
        self.number = Receipt.orders
        Receipt.orders += 1

    def receipt_print(self, pizzas):
        order_quantity = random.randint(3, 6)
        print(f'Your order is {order_quantity} pizzas:')
        order_list = random.sample(pizzas, order_quantity)
        each_qty = random.randint(1, 6)
        for i in order_list:
            self.lines.append(ReceiptLine(i, each_qty))

    def __str__(self):
        return f'Receipt: {self.number}\n {self.datetime}\n' + '\n'.join([str(line) for line in self.lines])
