from pizzastore.constants.pizzas_info import pizzas
from pizzastore.objects.pizza import Pizza
from pizzastore.objects.receipt import Receipt


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class PizzaStore(metaclass=SingletonMeta):
    def __init__(self):
        self.pizzas = [Pizza(*item) for item in pizzas]
        self.receipts = []

    def print_menu(self):
        for item in self.pizzas:
            print(item)

    def new_receipt(self):
        r = Receipt()
        return r

    def filter_pizzas(self, money):
        filtered_by_price = [item for item in self.pizzas if item.price < money]
        return filtered_by_price

    def pizzas_print(self, pizzas_list):
        for i in pizzas_list:
            print(f'{i.idx:02}, {i.name:<15} {i.price:02} UAH. {i.description}')

    def pizzas_sorted_by_price(self):
        for item in sorted(self.pizzas, key=lambda item: item.price):
            sorted_by_price = f'Pizza: {item.idx:02}, {item.name:<15}, {item.price:02} UAH : {item.description}'
            print(sorted_by_price)

    if __name__ == "__main__":
        print(pizzas)
