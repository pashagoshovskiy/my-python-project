pizzas = [
    (0, "Margarita", 95, 'Simple pizza with cheese'),
    (1, "Mexican", 130, 'Hot meat pizza with jalapeno'),
    (2, "Peperoni", 120, 'Best pizza with salami'),
    (3, "Bavarian", 135, 'Pizza with sausages and mustard'),
    (4, "Vegan", 100, 'Vegan pizza with white sauce'),
    (5, "4Meat", 150, 'Pizza with 4 different meats'),
    (6, "4Cheese", 145, 'Pizza with 4 different cheeses'),
    (7, "Americana", 160, 'Meat pizza with fries'),
    (8, "Hawaii", 105, 'Chicken pizza with pineapple'),
    (9, "BBQ", 125, 'Meat pizza with BBQ sauce')
]