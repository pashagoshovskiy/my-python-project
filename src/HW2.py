# y = a*x**2 + b*x +c
a = int(input("enter a: "))
b = int(input("enter b: "))
c = int(input("enter c: "))
d = b ** 2 - 4 * a * c

if d < 0:
    print('Действительных корней нет')
elif d == 0:
    print(-(b / 2 * a))
else:
    print("x1: ", (-b + d ** 0.5) / 2 * a, "\nx2: ", (-b - d ** 0.5) / 2 * a)

# num reverse(last task)
g = int(input("Enter 3 digits number: "))
lastDigit = int(g % 10)
hundreds = lastDigit * 100
tens = int(g % 100 - lastDigit)
ones = int((g % 1000 - tens - lastDigit) / 100)
result = hundreds + tens + ones

print(result)
